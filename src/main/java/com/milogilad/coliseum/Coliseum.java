package com.milogilad.coliseum;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public final class Coliseum extends JavaPlugin {

	private Player findPlayer(String username) {
		Player p = null;
		getLogger().info("Looking for " + username);

		for (Player o : Bukkit.getServer().getOnlinePlayers()) {
			getLogger().info(o.getName());
			if (o.getName().equals(username)) {
				getLogger().info("Match!");
				p = o;
				break;
			}
		}

		return p;
	}

	private Player hasOutstandingDuelRequest(Player p) {
		Player challenger = null;
		if ((getConfig().getString("requested") == p.getName())) {
			challenger = findPlayer(getConfig().getString("challenger"));
		}

		return challenger;
	}

	private void createDuel(Player challenger, Player requested) {
		if (getConfig().getString("challenger") != "" || getConfig().getString("requested") != "") {
			challenger.sendMessage("Someone else is trying to start a duel! Wait 60 seconds and try again.");
			return;
		}
		requested.sendMessage(challenger.getDisplayName()
				+ " has challenged you to a duel! Do /duel to accept or wait 60 seconds to decline.");

		getConfig().set("challenger", challenger.getName());
		getConfig().set("requested", requested.getName());
		saveConfig();

		challenger.sendMessage("Sent duel request to " + requested.getDisplayName());
	}

	private void duel(Player a, Player b) {
		// Teleport players to opposite ends of arena
		a.teleport(new Location(a.getWorld(), getConfig().getDouble("entrance.a.x"),
				getConfig().getDouble("entrance.one.y"), getConfig().getDouble("entrance.one.z")));

		b.teleport(new Location(b.getWorld(), getConfig().getDouble("entrance.b.x"),
				getConfig().getDouble("entrance.b.y"), getConfig().getDouble("entrance.b.z")));
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		Player p;

		if (sender instanceof Player) {
			p = (Player) sender;
		} else {
			sender.sendMessage("NPCs cannot use this plugin.");
			return false;
		}

		if (cmd.getName().equalsIgnoreCase("duel")) {
			if (!sender.hasPermission("coliseum.duel")) {
				sender.sendMessage("You're not permitted to use that command!");
				return false;
			}

			Player duelRequester = hasOutstandingDuelRequest(p);
			if (duelRequester != null) {
				duel(p, duelRequester);
			}

			createDuel(p, findPlayer(args[0]));
			return true;
		}

		return false;
	}

}
