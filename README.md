# Coliseum

Two players get to duke it out. The original, tried-and-true way to settle any kind of dispute, from infidelity to diamond-thievery!

## Set-Up

*This is a Bukkit plugin.*

1. Go to [this page](https://gitlab.com/Myl0g/coliseum/tags).
2. On the right side, click on the cloud with the downward arrow, and click on the last option in the menu that opens.
3. Move the .jar file into your plugins folder.
4. Restart your server.

Under the plugins folder, there should be a new "Coliseum" folder. Go to it, and see this portion:

```yml
entrance:
  a:
    x: 0.0
    y: 0.0
    z: 0.0
  b:
    x: 0.0
    y: 0.0
    z: 0.0
```

Change the `0.0`s under `a` to the coordinates of one side of your battle arena, and do the same for the other side with `b`.

That's it! You're all set up.

## Usage

`/duel [player]` to create a new duel, or

`/duel` to accept a challenge.
